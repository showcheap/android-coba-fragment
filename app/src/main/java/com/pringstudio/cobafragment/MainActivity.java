package com.pringstudio.cobafragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set default view for home view
        setView(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.home:
                setView(0);
                return true;
            case R.id.about:
                setView(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Fragment manager
    private void setView(int view){

        Log.d("setView","Mencoba ganti view ke : "+view);

        // Fragment transaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;

        // Case View
        switch (view){
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new AboutFragment();
                break;
        }

        // Implement fragment view
        if(fragment != null){
            transaction.replace(R.id.main_frame, fragment);
            transaction.commit();
        }
    }
}
